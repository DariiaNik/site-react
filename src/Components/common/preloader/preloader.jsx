import React from "react";
import './preloader.scss'

let Preloader = (props) => {
    return (
        <div className="lds-ellipsis">
            <div/>
            <div/>
            <div/>
            <div/>
        </div>
    )

}

export default Preloader;